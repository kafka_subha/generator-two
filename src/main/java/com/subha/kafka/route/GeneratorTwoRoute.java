package com.subha.kafka.route;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

/**
 * Created by subha on 03/07/2018.
 */
@Component
public class GeneratorTwoRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        errorHandler(deadLetterChannel("log:genTwo?level=ERROR&showHeaders=true&showBody=true")
                .useOriginalMessage());

        from("kafka:{{kafka.from-topic}}?brokers={{kafka.server}}:{{kafka.port}}&groupId={{kafka.channel}}&autoOffsetReset=earliest&consumersCount=1")
                .routeId("generator-two")
                .to("bean:genTwo?method=process")
                .to("kafka:{{kafka.to-topic}}?brokers={{kafka.server}}:{{kafka.port}}");
    }
}
