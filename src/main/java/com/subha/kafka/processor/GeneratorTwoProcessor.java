package com.subha.kafka.processor;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by subha on 03/07/2018.
 */

@Component("genTwo")
public class GeneratorTwoProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(GeneratorTwoProcessor.class);
    public void process(Exchange exchange) {
        Long seed = Long.parseLong(exchange.getIn().getBody().toString().split("-")[0]);
        Long generatedResult = 10 * seed + seed;
        LOG.info("Got seed " + seed);
        LOG.info("Generator one generated number " + generatedResult);
        exchange.getIn().setBody(generatedResult + "-GEN2-" + exchange.getIn().getBody().toString().split("-")[1]);

    }

}
